#include "EuclideanVector.h"
namespace cs6771 {
    EuclideanVector::EuclideanVector(unsigned int dimension,double magnitude)
        :mDimension(dimension),norm(-1)
    {
        value_ = new double[mDimension];
        for(unsigned int i = 0;i < mDimension;i ++)
            value_[i] = magnitude;

    }

    EuclideanVector::EuclideanVector(const EuclideanVector& e)
        :mDimension(e.mDimension),norm(-1)
    {
        value_ = new double[mDimension];
        for(unsigned int i = 0;i < mDimension;i ++)
            value_[i] = e.value_[i];

    }

    EuclideanVector::EuclideanVector(EuclideanVector&& e) noexcept
        :mDimension(std::move(e.mDimension)),value_{std::move(e.value_)},norm(-1)
        {
            e.value_ = nullptr;
        }

    EuclideanVector& EuclideanVector::operator=(const EuclideanVector& e)
    {
        if(value_ != nullptr)
            delete[] value_;
        mDimension = e.mDimension;
        value_ = new double[mDimension];
        norm = e.norm;
        for(unsigned int i = 0;i < mDimension;i ++)
            value_[i] = e.value_[i];
        return *this;

    }

    EuclideanVector& EuclideanVector::operator=(EuclideanVector&& e)
    {
        if(this != &e)
            delete[] value_;
        value_ = std::move(e.value_);
        mDimension = std::move(e.mDimension);
        norm = std::move(e.norm);
        e.value_ = nullptr;
        return *this;

    }

    EuclideanVector::~EuclideanVector(){
        delete[] value_;

    }

    int EuclideanVector::getNumDimensions()
    {
        return mDimension;

    }

    double EuclideanVector::get(unsigned int index)
    {
        return value_[index];

    }

    double EuclideanVector::getEuclideanNorm()
    {
        if (norm < 0 ) {
            double sum = 0;
            for (unsigned int i = 0;i < mDimension;i ++)
            {
                double a = value_[i];
                sum += a * a;

            }
            norm = sqrt(sum);

        }
        return norm;

    }
    EuclideanVector& EuclideanVector::createUnitVector()
    {
        EuclideanVector *e = new EuclideanVector(*this);

        double norm = this->getEuclideanNorm();
        for (unsigned int i = 0; i < e->mDimension; i++) {
            e->value_[i] /= norm;

        }

        return *e;

    }

    double& EuclideanVector::operator[](int i)
    {
        return value_[i];

    }

    EuclideanVector& EuclideanVector::operator+=(const EuclideanVector& rhs)
    {
        for (unsigned int i = 0; i < mDimension; i++) {
            value_[i] +=rhs.value_[i];

        }
        return *this;

    }

    EuclideanVector operator+(const EuclideanVector& lhs,const EuclideanVector& rhs)
    {
        EuclideanVector v = lhs;
        for (unsigned int i = 0; i < v.mDimension; i++) {
            v.value_[i] +=rhs.value_[i];

        }

        return v;

    }

    EuclideanVector& EuclideanVector::operator-=(const EuclideanVector& rhs)
    {
        for (unsigned int i = 0; i < mDimension; i++) {
            value_[i] -=rhs.value_[i];

        }
        return *this;

    }
    EuclideanVector operator-(const EuclideanVector& lhs,const EuclideanVector& rhs)
    {
        EuclideanVector v = lhs;
        for (unsigned int i = 0; i < v.mDimension; i++) {
            v.value_[i] -=rhs.value_[i];
        }
        return v;
    }

    EuclideanVector& EuclideanVector::operator*=(int v)
    {
        for (unsigned int i = 0; i < mDimension; i++) {
            value_[i] *= v;

        }
        return *this;

    }



    EuclideanVector& EuclideanVector::operator/=(const EuclideanVector& rhs)
    {
        for (unsigned int i = 0; i < mDimension; i++) {
            value_[i] /=rhs.value_[i];

        }
        return *this;

    }

    EuclideanVector operator/(const EuclideanVector& lhs,double s)
    {
        EuclideanVector v = lhs;
        for (unsigned int i = 0; i < v.mDimension; i++) {
            v.value_[i] /= s;

        }

        return v;

    }

    double operator*(const EuclideanVector& lhs,const EuclideanVector& rhs)
    {
        double sum = 0;
        for (unsigned int i = 0; i < lhs.mDimension; i++) {
            sum += lhs.value_[i] *rhs.value_[i];

        }

        return sum;

    }
    EuclideanVector operator*(double i,const EuclideanVector& rhs)
    {
        EuclideanVector v{rhs};
        for (unsigned int i = 0; i < v.mDimension; i++)
            v.value_[i] *= i;
        return v;

    }

    EuclideanVector operator*(const EuclideanVector& lhs,double i)
    {
        EuclideanVector v{lhs};
        for (unsigned int i = 0; i < v.mDimension; i++)
            v.value_[i] *= i;
        return v;

    }
    std::ostream& operator<<(std::ostream& cout,const EuclideanVector & e)
    {
        double* p = e.value_;
        cout <<"[";
        for (unsigned int i = 0; i < e.mDimension;i++,p ++) {
            cout<< *p;
            if (i+1 != e.mDimension)
                cout<< " ";

        }
        cout <<"]" ;
        return cout;

    }

    bool operator==(const EuclideanVector& lhs,const EuclideanVector& rhs)
    {
        if (lhs.mDimension == rhs.mDimension)
        {
            for (unsigned int i = 0; i < lhs.mDimension; i++) {
                if (lhs.value_[i] != rhs.value_[i])
                    return false;

            }
            return true;

        }

        return false;

    }
    bool operator!=(const EuclideanVector& lhs,const EuclideanVector& rhs){
        return !(lhs == rhs);

    }

}

