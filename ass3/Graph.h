#ifndef __GRAPH_H__
#define __GRAPH_H__
#include <string>
#include <utility>
#include <memory>
#include <iostream>
#include <algorithm>
#include <functional>
#include <cassert>
namespace cs6771 {

    using std::shared_ptr;
    using std::weak_ptr;
    using std::pair;
    using std::cout;
    using std::endl;

    template<typename N, typename E> class Graph;

    //Node Iterator Declaration Begin
    template<typename N, typename E> class Node_Iterator {
        public:
            typedef std::ptrdiff_t difference_type;
            typedef std::forward_iterator_tag iterator_category;
            typedef N value_type;
            typedef N* pointer;
            typedef N& reference;

            reference operator*() const;
            pointer operator->() const {
                return &(operator*());
            }
            Node_Iterator& operator++();
            Node_Iterator& operator++(int i);
            bool operator==(const Node_Iterator& other) const;
            bool operator!=(const Node_Iterator& other) const {
                return !operator==(other);
            }

            Node_Iterator() {
            }
            ;
            //when iterator create,it will pointe the node which hold the most edges
            //if two node's edges are same then comparation the value
            Node_Iterator(weak_ptr<typename Graph<N, E>::Node> pointee) :
                head_(pointee), pointee_(pointee) {
                    auto spH = pointee_.lock();
                    if (spH == nullptr)
                        return;

                    while (spH->next_) {
                        spH = spH->next_;
                        if (spH->edg_num > pointee_.lock()->edg_num)
                            pointee_ = spH;
                        else if (spH->edg_num == pointee_.lock()->edg_num
                                && spH->value_ < pointee_.lock()->value_)
                            pointee_ = spH;
                    }
                }
            ;
        private:
            weak_ptr<typename Graph<N, E>::Node> head_;
            weak_ptr<typename Graph<N, E>::Node> pointee_;
    };
    //Node Iterator End

    //Edge Iterator Declaration  Begin
    template<typename N, typename E> class Edge_Iterator {
        public:
            typedef std::ptrdiff_t difference_type;
            typedef std::forward_iterator_tag iterator_category;
            typedef pair<N, E> value_type;
            typedef pair<N, E>* pointer;
            typedef pair<N, E> reference;

            reference operator*() const;
            pointer operator->() const {
                auto p = new pair<N, E> { operator*() };
                return p;
            }
            Edge_Iterator& operator++();
            Edge_Iterator& operator++(int i);
            bool operator==(const Edge_Iterator& other) const;
            bool operator!=(const Edge_Iterator& other) const {
                return !operator==(other);
            }

            Edge_Iterator() {
            }
            ;
            //When edge iterator created,it will point the least weight
            //if the two edge are same then comparation the conn_node value
            Edge_Iterator(weak_ptr<typename Graph<N, E>::Node> wp_N) :
                wp_node_(wp_N), pointee_(wp_N.lock()->edge_) {
                    auto spH = pointee_;

                    while (spH.lock()->next_) {
                        spH = spH.lock()->next_;
                        if (spH.lock()->value_ < pointee_.lock()->value_)
                            pointee_ = spH;
                        else if (spH.lock()->value_ == pointee_.lock()->value_
                                && spH.lock()->conn_node_.lock()->value_
                                < pointee_.lock()->conn_node_.lock()->value_)
                            pointee_ = spH;
                    }
                }
            ;
        private:
            weak_ptr<typename Graph<N, E>::Node> wp_node_;
            weak_ptr<typename Graph<N, E>::Edge> pointee_;
    };
    //Edge Iterator End

    //Class Graph Declaration Begin
    template<typename N, typename E>
        class Graph {
            friend class Node_Iterator<N, E> ;
            friend class Edge_Iterator<N, E> ;
            typedef Node_Iterator<N, E> n_iterator;
            typedef Edge_Iterator<N, E> e_iterator;
            public:

            //constructor
            Graph() :
                sp_nhead_(nullptr) {
                }
            ;
            //copy constructor
            Graph(const Graph& graph) :
                sp_nhead_(new Graph<N, E>::Node(*graph.sp_nhead_.get())) {
                    auto ptr = graph.sp_nhead_;
                    while (ptr) {
                        auto edge = ptr->edge_;
                        while (edge) {
                            addEdge(ptr->value_, edge->conn_node_.lock()->value_,
                                    edge->value_);
                            edge = edge->next_;
                        }
                        ptr = ptr->next_;
                    }
                }
            ;

            //copy assignment
            Graph& operator=(const Graph& graph) {
                clear();
                auto node = new Graph<N, E>::Node(*graph.sp_nhead_.get());
                sp_nhead_ = std::make_shared<Graph<N, E>::Node>(*node);
                auto ptr = graph.sp_nhead_;
                while (ptr) {
                    auto edge = ptr->edge_;
                    while (edge) {
                        addEdge(ptr->value_, edge->conn_node_.lock()->value_,
                                edge->value_);
                        edge = edge->next_;
                    }
                    ptr = ptr->next_;
                }
                return *this;
            }
            ;

            //move constructor
            Graph(Graph&& graph) noexcept
                :sp_nhead_(std::move(graph.sp_nhead_)) {
                    graph.sp_nhead_ = nullptr;
                };

            //move assignment
            Graph& operator=(Graph&& graph) noexcept
            {
                clear();
                sp_nhead_ = std::move(graph.sp_nhead_);
                graph.sp_nhead_ = nullptr;
            };
            bool addNode(const N& n);
            bool addEdge(const N& o,const N& d,const E& v);
            bool isNode(const N& n)const;
            bool replace(const N& o,const N& d)
            {
                //check original node
                bool ret = isNode(o);
                if(!ret)
                    throw std::runtime_error("Can not find original node");

                //check replacement node
                ret = isNode(d);
                if(ret)
                    return false;

                //replace the value
                shared_ptr<Graph<N,E>::Node> sp_o = getNode(o);
                sp_o->value_ = d;
                return true;
            };
            void deleteNode(const N& n) noexcept;

            void deleteEdge(const N& o, const N& d, const E& v) noexcept;
            //clear the node information and the edge which relavent to each
            void clear() noexcept
            {
                sp_nhead_.reset();
            };

            void mergeReplace(const N& o,const N& d);
            bool isConnected(const N& o, const N& d) const;
            void printNodes() const;
            void printEdges(const N& n) const;
            //create an nodeiterator which point to the head
            n_iterator begin() const {return n_iterator(sp_nhead_);};
            //create an nodeiterator which point to the tail
            n_iterator end() const {return n_iterator();};
            //create an edgeiterator which point to the head
            e_iterator edgeIteratorBegin(const N& n) const
            {
                auto sp = sp_nhead_;
                while (sp)
                {
                    if(sp->value_ == n)
                        break;
                    sp =sp->next_;
                }
                return e_iterator(sp);
            };
            //create an edgeiterator which point to the tail
            e_iterator edgeIteratorEnd() const {return e_iterator();};

            private:
            struct Node;
            struct Edge;
            //struct Node declaration
            struct Node
            {
                friend class Graph<N,E>;
                Node(const N& value)
                    :value_(value),next_(nullptr),edge_(nullptr),edg_num(0) {};
                //copy constructor
                Node(Node& node)
                    :value_(node.value_),
                    next_(node.next_?new Graph<N,E>::Node(*node.next_.get()):nullptr),
                    edg_num(node.edg_num)
                {

                };

                //copy assignment
                Node& operator=(const Node& node)
                {
                    if(next_)
                        next_.reset();
                    if(edge_)
                        edge_.reset();
                    value_ = node.value_;
                    next_ = node.next_? new Graph<N,E>::Node(*node.next_.get()):nullptr;
                    edge_ = node.edge_? new Graph<N,E>::Edge(*node.edge_.get()):nullptr;
                    edg_num = node.edg_num;
                    return *this;
                };
                //move constructor
                Node(Node&& node) noexcept
                    :value_(std::move(node.value_)),next_(std::move(node.next_)),edge_(std::move(node.edge_)),edg_num(std::move(node.edg_num))
                    {
                        node.value_ = nullptr;
                        node.next_ = nullptr;
                        node.edge_ = nullptr;
                    };
                //move assignment
                Node& operator=(const Node&& node) noexcept
                {
                    if(next_)
                        next_.reset();
                    if(edge_)
                        edge_.reset();
                    value_ = std::move(node.value_);
                    next_ = std::move(node.next_);
                    edge_ = std::move(node.edge_);
                    edg_num = std::move(node.edg_num);
                    node.value_ = 0;
                    node.next_ = nullptr;
                    node.edge_ = nullptr;
                    return *this;
                };
                ~Node() {next_.reset();edge_.reset();};
                N value_;
                shared_ptr<Node> next_;
                shared_ptr<Edge> edge_;
                //identify connect edge number
                int edg_num;
            };
            //struct Node Declaration End

            //struct Edge Declaration Begin
            struct Edge
            {
                friend class Graph<N,E>;
                Edge(const E& value,weak_ptr<Node> conn):value_(value),next_(nullptr),conn_node_(conn) {};
                //copy constructor
                Edge(const Edge& edge)
                    :value_(edge.value_),
                    next_(edge.next_?new Graph<N,E>::Edge(*edge.next_.get()):nullptr){};

                //copy assignment
                Edge& operator=(const Edge& edge)
                {
                    value_ = 0;
                    if(next_)
                        next_.reset();
                    value_ = edge.value_;
                    next_ = new Graph<N,E>::Edge(*edge.next_.get());
                    conn_node_ = edge.conn_node_.lock();
                    return *this;
                };
                //move constructor
                Edge(Edge&& edge) noexcept
                    :value_(std::move(edge.value_)),next_(std::move(edge.next_)),conn_node_(std::move(edge.conn_node_))
                    {
                        edge.value_ = 0;
                        edge.next_ = nullptr;
                    };
                //move assignment
                Edge& operator=(Edge&& edge) noexcept
                {
                    value_ = 0;
                    if(next_)
                        next_.reset();
                    value_ = std::move(edge.value_);
                    next_ = std::move(edge.next_);
                    conn_node_ = std::move(edge.conn_node_.lock());

                    edge.value_ = 0;
                    edge.next_ = nullptr;
                    return *this;
                }
                ~Edge() {next_.reset();};
                E value_;
                shared_ptr<Edge> next_;
                weak_ptr<Node> conn_node_;
            };

            shared_ptr<Node> sp_nhead_;
            private:
            //input a N value output a Node share pointer
            shared_ptr<Node> getNode(const N& n) const
            {
                shared_ptr<Node> sp;
                auto sp_n = sp_nhead_;
                while(sp_n)
                {
                    if(sp_n->value_ == n)
                    {
                        sp = sp_n;
                        break;
                    }
                    sp_n = sp_n->next_;
                }
                return sp;
            }
        };
    //Class Graph End

    //NodeIterator Implementation
    template<typename N, typename E> typename Node_Iterator<N, E>::reference Node_Iterator<
        N, E>::operator*() const {
            return pointee_.lock()->value_;
        }

    template<typename N, typename E> Node_Iterator<N, E>&
        Node_Iterator<N, E>::operator++(int i) {
            auto self = *this;
            ++*this;
            return self;
        }

    template<typename N, typename E> Node_Iterator<N, E>&
        Node_Iterator<N, E>::operator++() {
            auto spCurrent = pointee_.lock();
            auto spH = head_.lock();

            // find the next node which edge number just lease than this
            weak_ptr<typename Graph<N, E>::Node> next;
            while (spH) {
                if (spCurrent->edg_num > spH->edg_num
                        || (spCurrent->edg_num == spH->edg_num
                            && spCurrent->value_ < spH->value_)) {
                    if (next.lock() == nullptr) {
                        next = spH;
                    } else if ((spH->edg_num > next.lock()->edg_num
                                || (spH->edg_num == next.lock()->edg_num
                                    && spH->value_ < next.lock()->value_))) {
                        next = spH;
                    }
                }
                spH = spH->next_;
            }
            pointee_ = next;

            return *this;
        }

    template<typename N, typename E>
        bool Node_Iterator<N, E>::operator==(const Node_Iterator<N, E>& other) const {
            if(this->pointee_.lock() == nullptr || other.pointee_.lock() == nullptr)
                return this->pointee_.lock() == other.pointee_.lock();
            else
                return this->pointee_.lock() == other.pointee_.lock()
                    || this->pointee_.lock()->value_ == other.pointee_.lock()->value_;
        }

    //Edge_Iterator Implement
    template<typename N, typename E> typename Edge_Iterator<N, E>::reference Edge_Iterator<
        N, E>::operator*() const {
            return std::make_pair(pointee_.lock()->conn_node_.lock()->value_,
                    pointee_.lock()->value_);
        }

    template<typename N, typename E> Edge_Iterator<N, E>&
        Edge_Iterator<N, E>::operator++(int i) {
            auto self = *this;
            ++*this;
            return self;
        }

    template<typename N, typename E> Edge_Iterator<N, E>&
        Edge_Iterator<N, E>::operator++() {
            auto spCurrent = pointee_.lock();
            auto spH = wp_node_.lock()->edge_;

            // find the next edge which weight just higher than this
            weak_ptr<typename Graph<N, E>::Edge> next;
            while (spH) {
                if (spCurrent->value_ < spH->value_
                        || (spCurrent->value_ == spH->value_
                            && spCurrent->conn_node_.lock()->value_
                            > spH->conn_node_.lock()->value_)) {
                    if (next.lock() == nullptr) {
                        next = spH;
                    } else if ((spH->value_ < next.lock()->value_
                                || (spH->value_ == next.lock()->value_
                                    && spH->conn_node_.lock()->value_
                                    > next.lock()->conn_node_.lock()->value_))) {
                        next = spH;
                    }
                }
                spH = spH->next_;
            }
            pointee_ = next;

            return *this;
        }

    template<typename N, typename E>
        bool Edge_Iterator<N, E>::operator==(const Edge_Iterator<N, E>& other) const {
            if(this->pointee_.lock() == nullptr || other.pointee_.lock() == nullptr)
                return this->pointee_.lock() == other.pointee_.lock();
            else
                return this->pointee_.lock() == other.pointee_.lock()
                    || this->pointee_.lock()->value_ == other.pointee_.lock()->value_;
        }

    //Graph Implement
    //add a N value in Graph as a node
    //node store in sp_nhead_ linklist
    template<typename N, typename E>
        bool Graph<N, E>::addNode(const N& n) {
            if (!isNode(n)) {
                if (sp_nhead_ != nullptr) {
                    //add new node in the tail
                    weak_ptr<Graph<N, E>::Node> wp = sp_nhead_;
                    while (wp.lock()->next_)
                        wp = wp.lock()->next_;

                    wp.lock()->next_ = std::make_shared<Graph<N, E>::Node>(n);
                } else {
                    //if sp_nhead_ does not initialization ,the new node is seen as head
                    sp_nhead_ = std::make_shared<Graph<N, E>::Node>(n);
                }
                return true;
            }
            return false;
        }

    template<typename N, typename E>
        bool Graph<N, E>::isNode(const N& n) const {
            //use iterator find the node
            auto i_n = std::find_if(begin(), end(), [&n](const N& node_value)
                    {
                    return node_value == n;
                    });
            if (i_n != end())
                return true;
            return false;
        }

    template<typename N, typename E>
        void Graph<N, E>::printNodes() const {
            //use the iterator traverse and print node value
            std::for_each(begin(), end(), [](const N& node_value)
                    {
                    cout << node_value << endl;
                    });
        }

    template<typename N, typename E>
        void Graph<N, E>::printEdges(const N& n) const {
            //finde relevent node
            shared_ptr<Graph<N, E>::Node> sp_o;
            sp_o = getNode(n);
            if (sp_o == nullptr)
                throw std::runtime_error("Can not find node");
            cout << "Edges attached to Node " << n << endl;
            if (sp_o->edge_ == nullptr) {
                cout << "(null)" << endl;
                return;
            }
            //use edge iterator traverse the edge and print
            std::for_each(edgeIteratorBegin(n), edgeIteratorEnd(),
                    [](const pair<N,E>& Epair)
                    {
                    cout <<Epair.first << " "<<Epair.second << endl;
                    });
        }

    template<typename N, typename E>
        void Graph<N, E>::deleteNode(const N& n) noexcept
        {
            if (!isNode(n))
                return;

            //delete node relevent edge first
            auto ptr = sp_nhead_;
            while (ptr) {
                auto ptr_edge = ptr->edge_;
                bool ishead = true;
                while (ptr_edge) {
                    if (ptr_edge->conn_node_.lock()->value_ == n && !ishead) {
                        deleteEdge(ptr->value_, n, ptr_edge->value_);
                    }
                    if (ptr_edge->conn_node_.lock()->value_ == n && ishead) {
                        deleteEdge(ptr->value_, n, ptr_edge->value_);
                        ptr_edge = ptr->edge_;
                        continue;
                    }
                    ishead = false;
                    ptr_edge = ptr_edge->next_;
                }
                ptr = ptr->next_;
            }
            //delete the node
            auto sp_o = getNode(n);
            if (sp_nhead_ == sp_o) {
                sp_nhead_ = sp_nhead_->next_;
                sp_o->next_ = nullptr;
                sp_o.reset();
            } else {
                auto sp_o_f = sp_nhead_;
                while (sp_o_f->next_) {
                    if (sp_o_f->next_ == sp_o) {
                        sp_o_f->next_ = sp_o_f->next_->next_;
                        sp_o->next_ = nullptr;
                        sp_o.reset();
                        break;
                    }
                    sp_o_f->next_ = sp_o_f->next_->next_;
                }
            }
        }
    ;
    template<typename N, typename E>
        void Graph<N, E>::deleteEdge(const N& o, const N& d, const E& v) noexcept
        {
            auto sp_o = getNode(o);
            auto sp_edge = sp_o->edge_;
            decltype(sp_edge) sp_edge_f;
            while (sp_edge) {
                if (sp_edge->value_ == v && sp_edge->conn_node_.lock()->value_ == d)
                    break;
                sp_edge_f = sp_edge;
                sp_edge = sp_edge->next_;
            }
            if (sp_edge) {
                if (sp_edge_f) {
                    sp_edge_f->next_ = sp_edge->next_;
                    sp_edge->next_ = nullptr;
                    sp_edge.reset();
                } else {
                    sp_o->edge_ = sp_o->edge_->next_;
                    sp_edge->next_ = nullptr;
                    sp_edge.reset();
                }
                sp_o->edg_num--;
            }
        }
    ;
    //pull all node o information to d and delete node o
    template<typename N, typename E>
        void Graph<N, E>::mergeReplace(const N& o, const N& d) {
            //check both two whether exist
            shared_ptr<Graph<N, E>::Node> sp_o, sp_d;
            sp_d = getNode(d);
            sp_o = getNode(o);
            if (!sp_d || !sp_o)
                throw std::runtime_error("node does not exist");

            //merge edge
            auto d_edge = sp_o->edge_;

            while (d_edge) {
                //if Node A -> Node C
                //When merge A to C ,ignore the Edge between A to C
                if (d_edge->conn_node_.lock()->value_ == sp_d->value_)
                    continue;

                addEdge(sp_d->value_, d_edge->conn_node_.lock()->value_,
                        d_edge->value_);
                d_edge = d_edge->next_;
            }
            auto ptr = sp_nhead_;
            while (ptr) {
                auto ptr_edge = ptr->edge_;
                while (ptr_edge) {
                    if (ptr_edge->conn_node_.lock()->value_ == d) {
                        addEdge(ptr->value_, sp_d->value_, ptr_edge->value_);
                        deleteEdge(ptr->value_, sp_o->value_, ptr_edge->value_);
                    }
                    ptr_edge = ptr_edge->next_;
                }
                ptr = ptr->next_;
            }

            //delete d node and merge node value
            deleteNode(o);
            sp_o->value_ = d;

        }
    ;

    template<typename N, typename E>
        bool Graph<N, E>::isConnected(const N& o, const N& d) const {
            //check whether both exist
            shared_ptr<Graph<N, E>::Node> sp_o, sp_d;
            sp_o = getNode(o);
            sp_d = getNode(d);

            //check whether connect through edge
            if (sp_o && sp_d) {
                auto sp_edge = sp_o->edge_;
                //check edge
                while (sp_edge) {
                    if (sp_edge->conn_node_.lock() == sp_d)
                        return true;
                    sp_edge = sp_edge->next_;
                }
                return false;
            } else
                throw std::runtime_error("Nodes do not exist!!");

        }
    ;
    template<typename N, typename E>
        bool Graph<N, E>::addEdge(const N& o, const N& d, const E& v) {
            //check whether already exist both two node
            shared_ptr<Graph<N, E>::Node> sp_o, sp_d;
            sp_o = getNode(o);
            sp_d = getNode(d);

            if (sp_o && sp_d) {
                auto sp_edge = sp_o->edge_;
                //check edge
                while (sp_edge) {
                    if (sp_edge->value_ == v && sp_edge->conn_node_.lock() == sp_d)
                        return false;
                    sp_edge = sp_edge->next_;
                }

                //connect
                auto edge = std::make_shared<Edge>(v, sp_d);
                if (sp_o->edge_ == nullptr) {
                    sp_o->edge_ = edge;
                } else {
                    auto self_edge = sp_o->edge_;
                    while (self_edge->next_)
                        self_edge = self_edge->next_;

                    self_edge->next_ = edge;
                }
                sp_o->edg_num++;
                return true;
            } else
                throw std::runtime_error("Nodes do not exist!!");

            return true;

        }
}
#endif

