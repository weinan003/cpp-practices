#include <cmath>
#include <vector>
#include <list>
#include <iostream>
#ifndef EUCLIDEAN_VECTOR_H
#define EUCLIDEAN_VECTOR_H
namespace cs6771 {
    class EuclideanVector {
        friend std::ostream& operator<<(std::ostream&,const EuclideanVector &);
        friend bool operator==(const EuclideanVector& lhs,const EuclideanVector& rhs);
        friend bool operator!=(const EuclideanVector& lhs,const EuclideanVector& rhs);
        friend  EuclideanVector operator+(const EuclideanVector& lhs,const EuclideanVector& rhs);
        friend  EuclideanVector operator-(const EuclideanVector& lhs,const EuclideanVector& rhs);
        friend  double operator*(const EuclideanVector& lhs,const EuclideanVector& rhs);
        friend  EuclideanVector operator*(double i,const EuclideanVector& rhs);
        friend  EuclideanVector operator*(const EuclideanVector& lhs,double i);
        friend  EuclideanVector operator/(const EuclideanVector& lhs,double v);
        public:
        EuclideanVector(unsigned int dimension,double magnitude = 0.0);

        template<class InputIterator>
            EuclideanVector(InputIterator begin,InputIterator end)
            :mDimension(0),value_{nullptr},norm(-1)
            {
                for(InputIterator a = begin;a != end;a++)
                    mDimension++;

                value_ = new double[mDimension];

                int i = 0;
                while (begin != end)
                {
                    value_[i] = *begin;
                    begin++;
                    i++;

                }

            };

        EuclideanVector(const EuclideanVector& e);
        EuclideanVector(EuclideanVector&& e) noexcept;

        //copy assignment
        EuclideanVector& operator=(const EuclideanVector& a);
        //move assignment
        EuclideanVector& operator=(EuclideanVector&& a);
        //Destructor
        ~EuclideanVector();

        int getNumDimensions();
        double get(unsigned int index);
        double getEuclideanNorm();

        EuclideanVector& createUnitVector();

        double& operator[](int i);

        EuclideanVector& operator+=(const EuclideanVector& rhs);
        EuclideanVector& operator-=(const EuclideanVector& rhs);
        EuclideanVector& operator*=(int i);
        EuclideanVector& operator/=(const EuclideanVector& rhs);
        operator std::vector<double>() const
        {
            std::vector<double> v;
            v.reserve(mDimension);
            for (unsigned int i = 0; i < mDimension; i++) {
                v.push_back(value_[i]);
            }
            return v;
        };

        operator std::list<double>() const
        {
            std::list<double> v;
            for (unsigned int i = 0; i < mDimension; i++) {
                v.push_back(value_[i]);
            }
            return v;
        };

        private:
        unsigned int mDimension;
        double* value_;
        double norm;
    };

    std::ostream& operator<<(std::ostream&,const EuclideanVector &);
    bool operator==(const EuclideanVector& lhs,const EuclideanVector& rhs);
    bool operator!=(const EuclideanVector& lhs,const EuclideanVector& rhs);
    EuclideanVector operator+(const EuclideanVector& lhs,const EuclideanVector& rhs);
    EuclideanVector operator-(const EuclideanVector& lhs,const EuclideanVector& rhs);
    double operator*(const EuclideanVector& lhs,const EuclideanVector& rhs);
    EuclideanVector operator*(double i,const EuclideanVector& rhs);
    EuclideanVector operator*(const EuclideanVector& lhs,double i);
    EuclideanVector operator/(const EuclideanVector& lhs,double s);
}

#endif
